FROM python:3.6-alpine3.10

WORKDIR /app

COPY requirements.txt .

RUN \
 apk add --no-cache postgresql-client postgresql-libs && \
 apk add --no-cache --virtual build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del build-deps

EXPOSE 5000

COPY . .

ENTRYPOINT ["/app/docker-entrypoint.sh"]
