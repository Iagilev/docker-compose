#!/usr/bin/env sh

set -e

# VALUE USERDB
# VALUE DB
# VALUE HOSTDB
# VALUE PASSWDB
# VALUE POSTGRES_PASSWORD
# VALUE POSTGRES_USER

check_db() {
    result=1
    count=0
    maxCount=10
    while [ $count -lt $maxCount ]; do
        if ping -W 1 -w 1 -c 1 $HOSTDB >/dev/null 2>&1; then
          if PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$HOSTDB" -c "\q" >/dev/null 2>&1; then
              result=0
              break
          fi
        fi
        sleep 1
        let count=count+1
    done
    return $result
}

check_userdb() {
    resultdb=1
    if check_db; then
        if PGPASSWORD="$PASSWDB" psql -U "$USERDB" -h "$HOSTDB" -d "$DB" -c "\q" >/dev/null 2>&1; then
            resultdb=0
        fi
    fi
    return $resultdb
}

create_userdb() {
    if ! check_userdb; then
        strdb="CREATE DATABASE $DB;"
        struser="CREATE USER $USERDB WITH ENCRYPTED PASSWORD '$PASSWDB';"
        strgrant="GRANT ALL PRIVILEGES ON DATABASE $DB to $USERDB;"
        PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$HOSTDB" -c "$strdb"
        PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$HOSTDB" -c "$struser"
        PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$HOSTDB" -c "$strgrant"
    fi
}

create_userdb
export FLASK_APP="app.py"
export POSTGRESQL_URL="postgresql://$USERDB:$PASSWDB@$HOSTDB/$DB"
flask db upgrade
python3 app.py

